#!/usr/bin/env python3
import sys

filename_1 = sys.argv[1]
filename_2 = sys.argv[2]

dataname_1 = filename_1.split('.')[0].replace('XENTR_', '')
dataname_2 = filename_2.split('.')[0].replace('XENTR_', '')


def read_fasta(tmp_filename):
    rv = dict()
    f = open(tmp_filename, 'r')
    if tmp_filename.endswith('.gz'):
        import gzip
        f = gzip.open(tmp_filename, 'rt')
    for line in f:
        if line.startswith('>'):
            tmp_h = line.strip().lstrip('>')
            rv[tmp_h] = []
        else:
            rv[tmp_h].append(line.strip())
    f.close()
    return rv


seq_1 = read_fasta(filename_1)
seq_2 = read_fasta(filename_2)

seq2h_1 = dict()
seq2h_2 = dict()

seq_type_1 = dict()
seq_type_2 = dict()

for tmp_h1, tmp_list1 in seq_1.items():
    (tmp_symbol, tmp_acc) = tmp_h1.split("|")
    tmp_type = tmp_acc.split()[1]
    tmp_acc = tmp_acc.split()[0]
    seq_type_1[tmp_acc] = tmp_type

    tmp_seq_1 = ''.join(tmp_list1)
    if tmp_seq_1 not in seq2h_1:
        seq2h_1[tmp_seq_1] = dict()
    seq2h_1[tmp_seq_1][tmp_acc] = tmp_symbol

for tmp_h2, tmp_list2 in seq_2.items():
    (tmp_symbol, tmp_acc) = tmp_h2.split("|")
    tmp_type = tmp_acc.split()[1]
    tmp_acc = tmp_acc.split()[0]
    seq_type_2[tmp_acc] = tmp_type

    tmp_seq_2 = ''.join(tmp_list2)
    if tmp_seq_2 not in seq2h_2:
        seq2h_2[tmp_seq_2] = dict()
    seq2h_2[tmp_seq_2][tmp_acc] = tmp_symbol

f_out = open('%s-%s.compare.txt' % (dataname_1, dataname_2), 'w')

count_same_seq = 0
count_same_record = 0
count_identical = 0
count_type_change = 0
count_symbol_change = 0
count_acc_change = 0
count_only_1 = 0
count_only_2 = 0

for tmp_seq_1, tmp_1 in seq2h_1.items():
    tmp_acc_1 = ",".join(sorted(seq2h_1[tmp_seq_1].keys()))
    tmp_symbol_1 = ",".join(sorted(seq2h_1[tmp_seq_1].values()))
    tmp_type_1 = ','.join(seq_type_1[x] for x
                          in sorted(seq2h_1[tmp_seq_1].keys()))

    if tmp_seq_1 in seq2h_2:
        count_same_seq += 1
        # same seq
        tmp_acc_2 = ",".join(sorted(seq2h_2[tmp_seq_1].keys()))
        tmp_symbol_2 = ",".join(sorted(seq2h_2[tmp_seq_1].values()))
        tmp_type_2 = ','.join(seq_type_2[x] for x
                              in sorted(seq2h_1[tmp_seq_1].keys()))

        if tmp_acc_1 == tmp_acc_2:
            if tmp_symbol_1 == tmp_symbol_2:
                if tmp_type_1 == tmp_type_2:
                    count_same_record += 1
                else:
                    count_type_change += 1
                    f_out.write("TypeChange[%s|%s]\t%s=%s\t%s=%s\n" %
                                (tmp_symbol_1, tmp_acc_1, dataname_1,
                                 tmp_type_1, dataname_2, tmp_type_2))
            else:
                count_symbol_change += 1
                f_out.write("SymbolChange[%s]\t%s=%s\t%s=%s\n" %
                            (tmp_acc_1, dataname_1, tmp_symbol_1,
                             dataname_2, tmp_symbol_2))
        else:
            count_acc_change += 1
            f_out.write("AccessionChange\t%s=%s\t%s=%s\n" %
                        (dataname_1, tmp_acc_1, dataname_2, tmp_acc_2))
    else:
        count_only_1 += 1
        f_out.write("Only[%s]\t%s|%s\n" %
                    (dataname_1, tmp_symbol_1, tmp_acc_1))

for tmp_seq_2, tmp_2 in seq2h_2.items():
    if tmp_seq_2 not in seq2h_1:
        tmp_acc_2 = ",".join(sorted(seq2h_2[tmp_seq_1].keys()))
        tmp_symbol_2 = ",".join(sorted(seq2h_2[tmp_seq_1].values()))

        count_only_2 += 1
        f_out.write("Only[%s]\t%s|%s\n" %
                    (dataname_2, tmp_symbol_2, tmp_acc_2))
f_out.close()

sys.stderr.write("%s sequences: %d (NR: %d)\n" %
                 (dataname_1, len(seq_1), len(seq2h_1)))
sys.stderr.write("%s sequences: %d (NR: %d)\n" %
                 (dataname_2, len(seq_2), len(seq2h_2)))
sys.stderr.write("\n")
sys.stderr.write("Identical seqs: %d\n" % (count_same_seq))
sys.stderr.write("  Diff seqs in %s: %d\n" % (dataname_1, count_only_1))
sys.stderr.write("  Diff seqs in %s: %d\n" % (dataname_2, count_only_2))
sys.stderr.write("\n")
sys.stderr.write("Identical records:: %d\n" % (count_same_record))
sys.stderr.write("  Change types: %d\n" % count_type_change)
sys.stderr.write("  Change symbols: %d\n" % count_symbol_change)
sys.stderr.write("  Change accessions: %d\n" % count_acc_change)
