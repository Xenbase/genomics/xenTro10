#!/usr/bin/env python3
import sys
import os

filename_gene_info = 'XENTR_gene_info.active.tsv'
dirname_here = os.path.dirname(os.path.realpath(__file__))

filename_gtf = sys.argv[1]
filename_out = '%s.updated' % filename_gtf

symbol_list = dict()
f_gene_info = open(os.path.join(dirname_here, '..', filename_gene_info), 'r')
for line in f_gene_info:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0]
    tmp_ncbi_id = tokens [1]
    symbol_list[tmp_ncbi_id] = tmp_symbol
f_gene_info.close()

f_gtf = open(filename_gtf, 'r')
f_out = open(filename_out, 'w')
for line in f_gtf:
    tmp_line = line.strip()
    if tmp_line.startswith('#'):
        f_out.write("%s\n" % tmp_line)
        continue

    tokens = tmp_line.split("\t")
    str_except_tag = "\t".join(tokens[:8])

    tag_list = []
    tag_dict = dict()
    tmp_gene_id = 'NA'
    tmp_gene_name = 'NA'

    for tmp in tokens[8].split(';'):
        tmp = tmp.strip()
        if tmp == '':
            continue
            
        (tmp_k, tmp_v) = tmp.strip().split()
        tmp_v = tmp_v.replace('"', '')

        tag_list.append(tmp_k)
        tag_dict[tmp_k] = tmp_v
        if tmp_k == 'gene_id':
            tmp_gene_id = tmp_v
        elif tmp_k == 'gene_name':
            tmp_gene_name = tmp_v
    
    is_changed = -1
    if tmp_gene_id in symbol_list:
        if symbol_list[tmp_gene_id] != tmp_gene_name:
            tag_dict['gene_name'] = symbol_list[tmp_gene_id]
            sys.stderr.write("Update\t%s\t%s\t%s\n" % (tmp_gene_id, tmp_gene_name, symbol_list[tmp_gene_id]))
            is_changed = 1
    
    if is_changed > 0:
        new_tag = "; ".join(['%s "%s"' % (tmp_k, tag_dict[tmp_k]) for tmp_k in tag_list])
        f_out.write("%s\t%s\n" % (str_except_tag, new_tag))
    else:
        f_out.write("%s\n" % tmp_line)

f_gtf.close()
f_out.close()
