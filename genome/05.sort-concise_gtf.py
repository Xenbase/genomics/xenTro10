#!/usr/bin/env python3
import sys

filename_gtf = sys.argv[1]

filename_out = '%s.sorted' % filename_gtf
filename_log = '%s.sorted.log' % filename_gtf

def sort_chr(tmp_list):
    num_list = dict()
    chr_list = []
    for tmp in tmp_list:
        if tmp[3:].split('.')[0].isnumeric():
            num_list[tmp] = int(tmp[3:])
        else:
            chr_list.append(tmp)
    return sorted(num_list.keys(), key=num_list.get)+sorted(chr_list)

gene_list = dict()
tx_list = dict()

f_gtf = open(filename_gtf, 'r')
f_out = open(filename_out, 'w')
f_log = open(filename_log, 'w')
for line in f_gtf:
    tmp_line = line.strip()

    if tmp_line == '':
        continue
    if tmp_line.startswith('#'):
        if tmp_line != '##':
            f_out.write("%s\n" % tmp_line)
        continue

    tokens = tmp_line.split("\t")
    tmp_seq_id = tokens[0]
    tmp_mol_type = tokens[2]
    tmp_start_pos = int(tokens[3])

    tag_list = dict()
    for tmp in tokens[8].split(';'):
        tmp = tmp.strip()
        if tmp == '':
            continue

        # to handle gene names with a whitespace (those symbols should be upddated in the future)
        # (tmp_k, tmp_v, tmp_others) 
        tmp_tokens = tmp.split()
        tmp_k = tmp_tokens[0]
        tmp_v = tmp_tokens[1]
        tmp_v = tmp_v.strip().replace('"', '')
        tag_list[tmp_k] = tmp_v

    if tmp_seq_id not in gene_list:
        gene_list[tmp_seq_id] = {'gene_pos': dict(), 'gene_info': dict(), 'tx_pos': dict()}
        
    if 'gene_id' not in tag_list:
        sys.stderr.write("ERROR in gene_id: %s\n" % tmp_line)
        sys.exit(1)
    tmp_gene_id = tag_list['gene_id']

    if tmp_mol_type == 'gene':
        if tmp_gene_id not in gene_list[tmp_seq_id]['gene_pos']:
            gene_list[tmp_seq_id]['gene_pos'][tmp_gene_id] = tmp_start_pos
            gene_list[tmp_seq_id]['gene_info'][tmp_gene_id] = tmp_line
        else:
            sys.stderr.write("Duplicate: %s\n" % tmp_gene_id)
            
    else: 
        if 'transcript_id' not in tag_list:
            sys.stderr.write("ERROR in transcript_id: %s\n" % tmp_line)
            sys.exit(1)
        tmp_tx_id = tag_list['transcript_id']

        if tmp_gene_id not in gene_list[tmp_seq_id]['tx_pos']:
            gene_list[tmp_seq_id]['tx_pos'][tmp_gene_id] = dict()

        if tmp_tx_id not in tx_list:
            tx_list[tmp_tx_id] = {'tx_info': 'NA', 'exon': dict(), 'CDS': dict() }
        gene_list[tmp_seq_id]['tx_pos'][tmp_tx_id] = -1

        if tmp_mol_type == 'transcript':
            gene_list[tmp_seq_id]['tx_pos'][tmp_gene_id][tmp_tx_id] = tmp_start_pos
            tx_list[tmp_tx_id]['tx_info'] = tmp_line
        elif tmp_mol_type == 'exon':
            tx_list[tmp_tx_id]['exon'][tmp_line] = tmp_start_pos
        elif tmp_mol_type == 'CDS':
            tx_list[tmp_tx_id]['CDS'][tmp_line] = tmp_start_pos
        else:
            sys.stderr.write("Unknown mol_type: %s\n" % tmp_line)
f_gtf.close()

for tmp_chr in sort_chr(gene_list.keys()):
    for tmp_g_id in sorted(gene_list[tmp_chr]['gene_pos'].keys(), 
                           key=gene_list[tmp_chr]['gene_pos'].get):

        if tmp_g_id not in gene_list[tmp_chr]['tx_pos']:
            f_log.write("NoTx\t%s\n" % gene_list[tmp_chr]['gene_info'][tmp_g_id])
            continue

        f_out.write("%s\n" % gene_list[tmp_chr]['gene_info'][tmp_g_id])
        for tmp_tx_id in sorted(gene_list[tmp_chr]['tx_pos'][tmp_g_id].keys(),
                                key=gene_list[tmp_chr]['tx_pos'][tmp_g_id].get):
            f_out.write("%s\n" % tx_list[tmp_tx_id]['tx_info'])

            for tmp_e in sorted(tx_list[tmp_tx_id]['exon'].keys(), 
                                key=tx_list[tmp_tx_id]['exon'].get):
                f_out.write("%s\n" % tmp_e)

            for tmp_cds in sorted(tx_list[tmp_tx_id]['CDS'].keys(), 
                                  key=tx_list[tmp_tx_id]['CDS'].get):
                f_out.write("%s\n" % tmp_cds)
f_out.close()
f_log.close()
