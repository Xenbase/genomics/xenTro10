#!/usr/bin/env python3

import sys

filename_tx_fa = sys.argv[1]
filename_gtf = filename_tx_fa.replace('_tx.fa', '')

tx_info = dict()
gene2tx = dict()

f_gtf = open(filename_gtf, 'r')
for line in f_gtf:
    if line.startswith('#'):
        continue

    tokens = line.strip().split("\t")
    tmp_mol_type = tokens[2]

    attr_list = dict()
    for tmp in tokens[8].strip().split(';'):
        if tmp.strip() == '':
            continue
        (tmp_k, tmp_v) = tmp.strip().split()
        tmp_k = tmp_k.strip()
        tmp_v = tmp_v.replace('"', '')
        attr_list[tmp_k] = tmp_v

    if tmp_mol_type == 'transcript':
        tx_id = attr_list['transcript_id']
        gene_id = attr_list['gene_id']
        gene_name = 'NA'
        if 'gene_name' in attr_list:
            gene_name = attr_list['gene_name']
        tx_info[tx_id] = '%s|%s|%s' % (gene_name, tx_id, gene_id)
        
        if gene_id not in gene2tx:
            gene2tx[gene_id] = dict()
        gene2tx[gene_id][tx_id] = 1
f_gtf.close()

filename_out_base = filename_tx_fa.replace('.fa', '')
filename_out_all = '%s_annot_all.fa' % filename_out_base
filename_out_longest = '%s_annot_longest.fa' % filename_out_base

tx_len = dict()
tx_seq = dict()
f_out_all = open(filename_out_all, 'w')
f_fa = open(filename_tx_fa, 'r')
for line in f_fa:
    if line.startswith('>'):
        tx_id = line.strip().lstrip('>').split()[0]
        cds_info = "CDS=NA"
        if line.find('CDS=') >= 0:
            cds_info = line.strip().lstrip('>').split()[1]
        tx_len[tx_id] = 0
        tx_seq[tx_id] = []
        f_out_all.write('>%s %s\n' % (tx_info[tx_id], cds_info))
    else:
        tx_len[tx_id] += len(line.strip())
        tx_seq[tx_id].append(line.strip())
        f_out_all.write("%s\n" % line.strip())
f_fa.close()
f_out_all.close()

f_out_longest = open(filename_out_longest, 'w')
for tmp_g in gene2tx.keys():
    tmp_tx_list = sorted(gene2tx[tmp_g].keys(), key=tx_len.get)
    tmp_t_longest = tmp_tx_list[-1]
    f_out_longest.write(">%s\n" % tx_info[tmp_t_longest])
    f_out_longest.write("%s\n" % "\n".join(tx_seq[tmp_t_longest]))
f_out_longest.close()
