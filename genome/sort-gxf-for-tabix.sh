#!/bin/bash

GXF=$1
SORTED=$GXF".sorted.gz"

(grep "^#" $GXF; grep -v "^#" $GXF | sort -t"`printf '\t'`" -k1,1 -k4,4n) | bgzip > $SORTED

# then, run tabix
# $ tabix <GTF/GFF3 file>
