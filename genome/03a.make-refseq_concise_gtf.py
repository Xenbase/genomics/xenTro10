#!/usr/bin/env python3
import sys

filename_gtf = sys.argv[1]

f_gtf = open(filename_gtf, 'r')
for line in f_gtf:
    if line.startswith('#'):
        print(line.strip())
        continue
    
    tokens = line.strip().split("\t")
    str_coord = "\t".join(tokens[:8])

    attr_list = dict()
    for tmp_attr in tokens[8].split(';'):
        tmp_attr = tmp_attr.strip()
        if tmp_attr.startswith('db_xref "GeneID:'):
            attr_list['gene_id'] = tmp_attr.split()[1].replace('"', '')
        if tmp_attr.startswith('gene '):
            attr_list['gene_name'] = tmp_attr.split()[1].replace('"', '')
        if tmp_attr.startswith('transcript_id '):
            attr_list['transcript_id'] = tmp_attr.split()[1].replace('"', '')
        if tmp_attr.startswith('protein_id '):
            attr_list['protein_id'] = tmp_attr.split()[1].replace('"', '')
        if tmp_attr.startswith('exon_number '):
            attr_list['exon_number'] = tmp_attr.split()[1].replace('"', '')
    
    if attr_list['gene_id'].find('unassigned') >= 0 or attr_list['transcript_id'].find('unassigned') >= 0:
        sys.stderr.write('Skip %s\n' % line.strip())
        continue

    tmp_mol_type = tokens[2]
    tmp_new_attr = ''
    
    if tmp_mol_type == 'gene':
        tmp_new_attr = 'gene_id "%s";' % attr_list['gene_id']

    if tmp_mol_type == 'transcript':
        tmp_new_attr = 'transcript_id "%s"; gene_id "%s";' %\
                       (attr_list['transcript_id'], 
                        attr_list['gene_id'])

    if tmp_mol_type == 'exon':
        tmp_exon_id = '%s-exon%s' % (attr_list['transcript_id'],
                                     attr_list['exon_number'])
        tmp_new_attr = 'exon_id "%s"; exon_number "%s";' %\
                       (tmp_exon_id, attr_list['exon_number'])
        tmp_new_attr += ' transcript_id "%s"; gene_id "%s";' %\
                        (attr_list['transcript_id'],
                         attr_list['gene_id'])

    if tmp_mol_type == 'CDS':
        tmp_new_attr = 'protein_id "%s"; exon_number "%s";' %\
                       (attr_list['protein_id'], attr_list['exon_number'])
        tmp_new_attr += ' transcript_id "%s"; gene_id "%s";' %\
                        (attr_list['transcript_id'],
                         attr_list['gene_id'])

    if tmp_new_attr != '' and 'gene_name' in attr_list:
        tmp_new_attr = '%s gene_name "%s";' % (tmp_new_attr,
                                               attr_list['gene_name'])

    if tmp_new_attr != '':
        print("%s\t%s" % (str_coord, tmp_new_attr))

f_gtf.close()
