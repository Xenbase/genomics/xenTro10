#!/usr/bin/env python3
import sys
import datetime

date_ver = datetime.datetime.now().strftime("%Y-%m-%d")

filename_gene_info = '../XENTR_gene_info.active.tsv'
filename_prot_id_list = '../XENTR_prot_id_list.active.csv'

filename_out_base = 'XENTR_gene_info.sync.%s' % date_ver
f_log = open('%s.log' % filename_out_base, 'w')

xb2ncbi = dict()
ncbi2xb = dict()

xb_black_list = dict()
f_prot = open(filename_prot_id_list, 'r')
for line in f_prot:
    if line.startswith('#'):
        continue
    tokens = line.strip().split(",")
    ncbi_id = tokens[3]
    xb_id = tokens[4]
    if xb_id in xb2ncbi and xb2ncbi[xb_id] != ncbi_id:
        sys.stderr.write("MultiXB\t%s\t%s,%s\n" % (xb_id, ncbi_id, xb2ncbi[xb_id]))
        f_log.write("MultiXB\t%s\t%s,%s\n" % (xb_id, ncbi_id, xb2ncbi[xb_id]))
        xb_black_list[xb_id] = 1
    else:
        xb2ncbi[xb_id] = ncbi_id
    
    if ncbi_id in ncbi2xb and ncbi2xb[ncbi_id] != xb_id:
        sys.stderr.write("MultiNCBI\t%s\t%s,%s\n" % (ncbi_id, xb_id, ncbi2xb[ncbi_id]))
        f_log.write("MultiNCBI\t%s\t%s,%s\n" % (ncbi_id, xb_id, ncbi2xb[ncbi_id]))
    else:
        ncbi2xb[ncbi_id] = xb_id
f_prot.close()

report_xb_list = []
report_ncbi_list = []

f_out = open('%s.tsv' % filename_out_base, 'w')
f_gene = open(filename_gene_info, 'r')
for line in f_gene:
    if line.startswith('#'):
        f_out.write("%s\n" % line.strip())
        continue
    
    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0]
    ncbi_id = tokens[1]
    xb_id = tokens[2]
    tmp_name = tokens[3]

    if xb_id in xb_black_list:
        sys.stderr.write("MultiXB-skip\t%s\n" % line.strip())
        f_log.write("===\t%s\tMultiXB-skip\n" % line.strip())
        f_out.write("%s\n" % line.strip())
        report_ncbi_list.append(ncbi_id)
        report_xb_list.append(xb_id)
        continue

    if ncbi_id not in ncbi2xb:
        sys.stderr.write('NoNCBI\t%s\n' % line.strip())
        f_log.write("---\t%s\tNoNCBI\n" % line.strip())
        continue

    if xb_id not in xb2ncbi:
        sys.stderr.write('NoXB\t%s\n' % line.strip())
        f_log.write("---\t%s\tNoXB\n" % line.strip())
        continue
    
    f_out.write("%s\n" % line.strip())
    report_ncbi_list.append(ncbi_id)
    report_xb_list.append(xb_id)
f_gene.close()

for tmp_xb in xb2ncbi.keys():
    if tmp_xb not in report_xb_list:
        sys.stderr.write("AddXBGene\t%s\n" % tmp_xb)

for tmp_ncbi in ncbi2xb.keys():
    if tmp_ncbi not in report_ncbi_list:
        sys.stderr.write("AddNCBIGene\t%s\n" % tmp_ncbi)
f_log.close()
