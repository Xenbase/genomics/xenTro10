#!/usr/bin/env python3
import sys
import datetime

date_ver = datetime.datetime.now().strftime("%Y-%m-%d")

filename_gene_info = '../XENTR_gene_info.active.tsv'
filename_out_base = 'XENTR_gene_info.SYMBOL_NAME.%s' % date_ver

filename_gene_table = sys.argv[1]
# filename_gene_table = 'Xenbase_GPI.2023-07-10.XENTR.gene_table.tsv'

tbl_info = dict()
f_tbl = open(filename_gene_table, 'r')
for line in f_tbl:
    tokens = line.strip().split("\t")
    ncbi_id = tokens[0]
    xb_id = tokens[1]
    tmp_symbol = tokens[2].split()[0]  # remove "(provisional)"
    tmp_name = tokens[4]
    tbl_info[xb_id] = {'symbol': tmp_symbol, 'name': tmp_name}
f_tbl.close()

f_out = open("%s.tsv" % filename_out_base, 'w')
f_log = open("%s.log" % filename_out_base, 'w')

f_gene = open(filename_gene_info, 'r')
for line in f_gene:
    if line.startswith('#'):
        f_out.write("%s\n" % line.strip())
        continue

    tmp_line = line.strip()
    new_line = tmp_line
    tokens = tmp_line.split("\t")

    tmp_symbol = tokens[0]
    ncbi_id = tokens[1]
    xb_id = tokens[2]
    tmp_name = tokens[3]

    if xb_id in tbl_info:
        tbl_symbol = tbl_info[xb_id]['symbol']
        tbl_name = tbl_info[xb_id]['name']
        new_line = "%s\t%s\t%s\t%s" % (tbl_symbol, ncbi_id, xb_id, tbl_name)
    
    if tmp_symbol != tbl_symbol:
        if tmp_name != tbl_name:
            f_log.write("---\t%s\tChangeSymbolName\n" % tmp_line)
            f_log.write("+++\t%s\tChangeSymbolName\n" % new_line)
            f_out.write("%s\n" % new_line)
        else:
            f_log.write("---\t%s\tChangeSymbol\n" % tmp_line)
            f_log.write("+++\t%s\tChangeSymbol\n" % new_line)
            f_out.write("%s\n" % new_line)
    elif tmp_name != tbl_name:
        f_log.write("---\t%s\tChangeName\n" % tmp_line)
        f_log.write("+++\t%s\tChangeName\n" % new_line)
        f_out.write("%s\n" % new_line)
    else:
        f_out.write("%s\n" % tmp_line)
f_gene.close()
