# Integrated reference proteome of *Xenopus tropicalis*

## UniProt-NCBI_RefSeq integration

* Prepare non-redundant NCBI RefSeq proteome. See [this document](../ncbi/README.md) for more information.
  * Currentversion: RefSeq genome annotation release 104.
* Prepare non-redundant UniProt reference proteome. See [this document](../uniprot/README.md) for more information.
  * Current version: UniProt 2023_02

* Compare protein sequences from each proteome and link them ONLY IF their sequences are 100% identical.
  * Sequences of NCBI RefSeq (release 104): 37,591
  * Sequences of UniProt (2023_02): 37,651
  * Sequences matched in both RefSeq and UniProt: 37,416
  * Sequences only available at NCBI RefSeq: 175
    * [FASTA file](XENTR_xenTro10_refseq104-up202302.raw.prot_refseq104_only.fa)
  * Sequences only available at UniProt: 235
    * [FASTA file](XENTR_xenTro10_refseq104-up202302.raw.prot_up2023_02_only.fa)

* Initialize prot_id_list, having UniProt/RefSeq protein IDs with RefSeq rna ID, NCBI gene ID, and Xenbase XB-GENE-ID.
  * Sequences only available on NCBI RefSeq contain NCBI gene IDs which is not available in the matched protein sequences, so they are added also (with 'NA' for UniProt accession number). 

* Update XB-GENE-IDs, and create new XB-GENE-ID if it is not available.

* Update gene symbols from manually curated [gene_info table](../XENTR_gene_info.active.tsv)
