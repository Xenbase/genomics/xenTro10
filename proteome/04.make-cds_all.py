#!/usr/bin/env python3
import sys

filename_prot_fa = sys.argv[1]
# filename_prot_fa = 'XENTR_xb202306.prot_all.fa'
# >dhrs9|XP_012825725.1|XM_012970271.3|GeneID:407852|XB-GENE-973205
# MLLCFLIGVAILYIWWRVRDGLKINNITEKYILITGCDTGFGNHAAKTFDKQGFRILATC

filename_cds_fa = '../ncbi/XENTR_xenTro10.refseq104.cds_all.fa'
# >LOC101732307|cds-XP_031751054.1|XM_031895194.1|GeneID:101732307 xb_gene_id=NA
# ATGAGGTGGCTCTTACTTTTACTCTGGGGGGCAATGGGCCATGGATTGGTACGCGTGCCGCTGCAGAGAGGGACATCTCT

prot2h = dict()
f_prot = open(filename_prot_fa, 'r')
for line in f_prot:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>')
        tokens = tmp_h.split("|")
        prot_id = tokens[1]
        prot2h[prot_id] = "%s|cds-%s|%s" % (tokens[0], prot_id, "|".join(tokens[2:]))
f_prot.close()

is_print = -1
f_cds = open(filename_cds_fa, 'r')
for line in f_cds:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>')
        tmp_prot_id = tmp_h.split('|')[1].replace('cds-', '')
        if tmp_prot_id in prot2h:
            print(">%s" % prot2h[tmp_prot_id])
            is_print = 1
        else:
            is_print = -1
    elif is_print > 0:
        print(line.strip())
f_cds.close()

        
