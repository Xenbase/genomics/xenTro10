#!/usr/bin/env python3
import sys

filename_prot_id_list = sys.argv[1]

query_list = dict()
f_list = open(filename_prot_id_list, 'r')
for line in f_list:
    if line.startswith('#'):
        continue
    tokens = line.strip().split(',')
    ncbi_id = tokens[3]
    query_list[ncbi_id] = 1
f_list.close()

# 42Sp43	GeneID:548848	XB-GENE-5898342	P43 5S RNA-binding protein

f_gene = open('../ncbi/XENTR_gene_info.refseq104.tsv', 'r')
for line in f_gene:
    tokens = line.strip().split("\t")
    ncbi_id = tokens[1]
    if ncbi_id in query_list:
        print(line.strip())
f_gene.close()
