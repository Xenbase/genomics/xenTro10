#!/usr/bin/env python3
import sys

filename_prot_fa = sys.argv[1]
# filename_prot_fa = 'XENTR_XB202306.prot_all.fa'
# >dhrs9|XP_012825725.1|XM_012970271.3|GeneID:407852|XB-GENE-973205
# MLLCFLIGVAILYIWWRVRDGLKINNITEKYILITGCDTGFGNHAAKTFDKQGFRILATC

filename_cdna_fa = '../ncbi/XENTR_xenTro10.refseq104.tx_all.fa'
# >LOC101732307|XM_031895194.1|GeneID:101732307 xb_gene_id=NA
# ATGAGGTGGCTCTTACTTTTACTCTGGGGGGCAATGGGCCATGGATTGGTACGCGTGCCGCTGCAGAGAGGGACATCTCT

tx2h = dict()
f_prot = open(filename_prot_fa, 'r')
for line in f_prot:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>')
        tokens = tmp_h.split("|")
        tx_id = tokens[2]
        tx2h[tx_id] = "%s|%s" % (tokens[0], "|".join(tokens[2:]))
f_prot.close()

is_print = -1
f_cdna = open(filename_cdna_fa, 'r')
for line in f_cdna:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>')
        tmp_tx_id = tmp_h.split('|')[1]
        if tmp_tx_id in tx2h:
            print(">%s" % tx2h[tmp_tx_id])
            is_print = 1
        else:
            is_print = -1
    elif is_print > 0:
        print(line.strip())
f_cdna.close()

        
