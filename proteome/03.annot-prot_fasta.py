#!/usr/bin/env python3
import sys

filename_fa = sys.argv[1]

gene_info = dict()
f_list = open('../XENTR_prot_id_list.active.csv', 'r')
for line in f_list:
    if line.startswith('#'):
        continue
    tokens = line.strip().split(",")
    gene_id = tokens[3]
    if gene_id not in gene_info:
        gene_info[gene_id] = {'symbol': 'NA', 'xb': 'NA'}
    gene_info[gene_id]['xb'] = tokens[4]
f_list.close()
# UNIPROT_ACC,REFSEQ_PROT_ID,REFSEQ_RNA_ID,NCBI_GENE_ID,XB_GENE_ID
# A0A023W428,NP_001278991.1,NM_001292062.1,GeneID:100492429,XB-GENE-1216189

f_symbol = open('../XENTR_gene_info.active.tsv', 'r')
for line in f_symbol:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0]
    gene_id = tokens[1]
    if gene_id not in gene_info:
        gene_info[gene_id] = {'symbol': 'NA', 'xb': 'NA'}
    gene_info[gene_id]['symbol'] = tmp_symbol
f_symbol.close()

# GENE_SYMBOL	NCBI_GENE_ID	XB_GENE_ID	GENE_NAME
# 42sp43	GeneID:548848	XB-GENE-5898342	P43 5S RNA-binding protein

f_fa = open(filename_fa, 'r')
for line in f_fa:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>')
        tmp_ncbi_tokens = tmp_h.split()[0].split('|')
        tmp_gene_id = tmp_ncbi_tokens[-1]

        new_xb = 'NA'
        new_symbol = tmp_ncbi_tokens[0]
        new_xb = tmp_h.split()[1].split('=')[1]
        if tmp_gene_id in gene_info:
            new_symbol = gene_info[tmp_gene_id]['symbol']
            new_xb = gene_info[tmp_gene_id]['xb']
        else:
            sys.stderr.write("Not available: %s\n" % tmp_gene_id)

        new_h = '%s|%s|%s' % \
                (new_symbol, '|'.join(tmp_ncbi_tokens[1:]), new_xb)
        print(">%s" % new_h)
    else:
        print(line.strip())
f_fa.close()
