#!/usr/bin/env python3
import sys

filename_fa = sys.argv[1]

f_fa = open(filename_fa, 'r')
if filename_fa.endswith('.gz'):
    import gzip
    f_fa = gzip.open(filename_fa, 'rt')

gene2h = dict()
seq_list = dict()

for line in f_fa:
    if line.startswith('>'):
        tmp_h = line.strip().lstrip('>')
        seq_list[tmp_h] = []

        tmp_gene_id = tmp_h.split('|')[3]
        if tmp_gene_id not in gene2h:
            gene2h[tmp_gene_id] = []
        gene2h[tmp_gene_id].append(tmp_h)
    else:
        seq_list[tmp_h].append(line.strip())
f_fa.close()

for tmp_gene_id in sorted(gene2h.keys()):
    longest_h = ''
    longest_seqlen = 0
    for tmp_h in gene2h[tmp_gene_id]:
        tmp_seqlen = len(''.join(seq_list[tmp_h]))
        if longest_seqlen < tmp_seqlen:
            longest_seqlen = tmp_seqlen
            longest_h = tmp_h
    print(">%s\n%s" % (longest_h, "\n".join(seq_list[longest_h])))
